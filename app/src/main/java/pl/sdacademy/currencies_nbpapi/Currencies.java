package pl.sdacademy.currencies_nbpapi;

import android.app.Activity;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

/**
 * Created by RENT on 2017-06-26.
 */

public class Currencies extends AsyncTask<Void, Void, HashMap<String, String>> {


    public static final String CURRENCY = "currency";
    public static final String CODE = "code";
    public static final String GET = "GET";
    public static final String MID = "mid";

    @Override
    protected HashMap<String, String> doInBackground(Void... params) {

        HashMap<String, String> currencyMap = new HashMap<>();
        String[] tables = {"A", "B"};
        for (String value : tables) {

            HttpURLConnection http = null;
            URL url = null;
            try {
                url = new URL("http://api.nbp.pl/api/exchangerates/tables/" + value + "/");

                http = (HttpURLConnection) url.openConnection();

                http.setRequestMethod(GET);
                http.connect();
                int responseCode = http.getResponseCode();
                if (responseCode == 200) {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(http.getInputStream()));
                    String line = null;
                    StringBuilder stringBuilder = new StringBuilder();
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line);
                    }

                    parse(stringBuilder, currencyMap);


                } else {
                    System.out.println("Tabela nie obsłużona");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return currencyMap;
    }

    private static HashMap<String, String> parse(StringBuilder stringBuilder, HashMap<String, String> currencyMap) throws JSONException {

        JSONArray jsonArray = new JSONArray(stringBuilder.toString());
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            JSONArray ratesArray = (JSONArray) jsonObject.get("rates");
            for (int j = 0; j < ratesArray.length(); j++) {
                JSONObject o = (JSONObject) ratesArray.get(j);
                currencyMap.put(o.getString(CODE), o.getString(MID));

//                System.out.print(o.get(CODE));
//                System.out.print(" : ");
//                System.out.print(o.get(CURRENCY));
//                System.out.print(" : ");
//                System.out.println(o.get(MID));
            }
        }
        return currencyMap;
    }
}
